import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

let operationQueue = OperationQueue()

class OperationCanclTest: Operation {
    override func main() {
        if isCancelled {
            print(isCancelled)
            return
        }
        print("test 1")
        sleep(1)
        
        if isCancelled {
            print(isCancelled)
            return
        }
        print("test 2")
    }
}

func cancelOperationMethod() {
    let cancelOperation = OperationCanclTest()
    operationQueue.addOperation(cancelOperation)
    cancelOperation.cancel()
}

//cancelOperationMethod()

class WaitOPerationTestA {
    private var operationQueue = OperationQueue()
    
    func test() {
        operationQueue.addOperation {
            sleep(1)
            print("Test 1")
        }
        
        operationQueue.addOperation {
            sleep(2)
            print("Test 2")
        }
        
        operationQueue.waitUntilAllOperationsAreFinished()
        
        operationQueue.addOperation {
            sleep(2)
            print("Test 3")
        }
        
        operationQueue.waitUntilAllOperationsAreFinished()
        
        operationQueue.addOperation {
            print("Test 4")
        }
    }
}

//let waitOperationTestA = WaitOPerationTestA()
//waitOperationTest.test()

class WaitOPerationTestB {
    private let operationQueue = OperationQueue()
    
    func test() {
        let operation1 = BlockOperation {
            sleep(1)
            print("test 1")
        }
        let operation2 = BlockOperation {
            sleep(2)
            print("test 2")
        }
        
        operationQueue.addOperations([operation1, operation2], waitUntilFinished: true)
        
        let operation3 = BlockOperation {
            print("test 3")
        }
        
        operationQueue.addOperation(operation3)
    }
}

//let waitOperationTest2 = WaitOPerationTestB()
//waitOperationTest2.test()

class ComplitionBlockTest {
    private let operationQueue = OperationQueue()

    
    func test() {
        let operation1 = BlockOperation {
            sleep(2)
            print("test1")
        }
        
        operation1.completionBlock = {
            print("finish CompletionBlock of test1")
        }
        
//        operationQueue.addOperation(operation1)
        
        let operation2 = BlockOperation {
            sleep(2)
            print("test2")
        }
        
        operationQueue.addOperations([operation1, operation2], waitUntilFinished: true)
        
        let operation3 = BlockOperation {
            print("test operation3")
        }
        
        operation3.completionBlock = {
            print("finish CompletionBlock of test3")
        }
        
        operationQueue.addOperation(operation3)
    }
}

let complitionBlockTest = ComplitionBlockTest()
complitionBlockTest.test()
