import UIKit
import PlaygroundSupport

// MARK: - Try it in your MyNotes with DataBase-RealmSwift

PlaygroundPage.current.needsIndefiniteExecution = true
//
//var array = [Int]()
//
//for i in 0...9 {
//    array.append(i)
//}
//
//print(array)
//print(array.count)

//var array = [Int]()
//
//DispatchQueue.concurrentPerform(iterations: 10) { index in
//    array.append(index)
//}
//
//print(array)
//print(array.count)

class SafeArray<T: Equatable> {
    private var array = [T]()
    private var queue = DispatchQueue(label: "MyQueue", attributes: .concurrent)
    
    public func append(_ value: T) {
        queue.async(flags: .barrier) {
            self.array.append(value)
        }
    }
    public var valueArray: [T] {
        get {
            var result = [T]()
            queue.sync {
                result = self.array
            }
            return result
        }
    }
}

var arraySafe = SafeArray<Int>()
DispatchQueue.concurrentPerform(iterations: 10) { index in
    arraySafe.append(index)
}

print( arraySafe.append(1222), "\narrayValue = ", arraySafe.valueArray, arraySafe.append(344), "\narrayValue = ", arraySafe.valueArray)
