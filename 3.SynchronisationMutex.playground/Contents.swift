import UIKit

//class SaveThread {
//    private var mutex = pthread_mutex_t()
//
//    init() {
//        pthread_mutex_init(&mutex, nil)
//    }
//
//    func someMethod(completion: () -> ()) {
//        pthread_mutex_lock(&mutex) // block thread from changing
//
//        // some data
//        // Thread save from collision and other stuff
//        completion()
//        defer { // 100% will run in the end of all stuff in function
//            pthread_mutex_unlock(&mutex) // unblock thread from changing
//        }
//    }
//}

class SaveThread {
    private let lockMutex = NSLock()
    
    func someMethod(completion: () -> ()) {
        lockMutex.lock()
        completion()
        defer {
            lockMutex.unlock()
        }
    }
}

var arr = [String]()
let saveThread = SaveThread()

saveThread.someMethod {
    print("Test1")
    arr.append("1 thread")
}

arr.append("2 thread")
