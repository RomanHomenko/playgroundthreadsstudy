import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

//let operationA = {
//    print("Start")
//    print(Thread.current)
//    print("finish")
//}
//
//let queue = OperationQueue()
//queue.addOperation(operationA)

//print(Thread.current)
//
//var result: String?
//let concurrentOperationA = BlockOperation {
//    print(Thread.current)
//    result = "Operation Block"
//}
//
////concurrentOperationA.start()
////print(result!)
//
//let queue = OperationQueue()
//queue.addOperation(concurrentOperationA)
//sleep(1)
//print(result!)

//print(Thread.current)
//
//let queue = OperationQueue()
//queue.addOperation {
//    print("Something")
//    print(Thread.current)
//}

class MyThread: Thread {
    override func main() {
        print("test override main thread")
    }
}

//let myThread = MyThread()
//myThread.start()

print(Thread.current)

class OperationA: Operation {
    override func main() {
        print("override main in operation A")
        print(Thread.current)
    }
}

let operationA = OperationA()
//operationA.start()
let queueA = OperationQueue()
queueA.addOperation(operationA)
