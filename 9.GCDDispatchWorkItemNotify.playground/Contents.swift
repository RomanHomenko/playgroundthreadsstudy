import UIKit
import PlaygroundSupport
import Foundation

PlaygroundPage.current.needsIndefiniteExecution = true

class DispatchWorkItem1 {
    private let queue = DispatchQueue(label: "DispatchWorkItem1", attributes: .concurrent)
    
    func create() {
        let workItem = DispatchWorkItem {
            print(Thread.current)
            print("Start task")
        }
        
        workItem.notify(queue: .main) {
            print(Thread.current)
            print("Task finished")
        }
        
        queue.async(execute: workItem)
    }
}

//let propDispWorkItem = DispatchWorkItem1()
//propDispWorkItem.create()

class DispatchWorkItem2 {
    private let queue = DispatchQueue(label: "DispatchWorkItem2")
    
    func create() {
        queue.async {
            sleep(3)
            print(Thread.current)
            print("Task1")
        }
        
        queue.async {
            sleep(3)
            print(Thread.current)
            print("Task2")
        }
        
        let workItem = DispatchWorkItem {
            print(Thread.current)
            print("Start work item task 3")
        }
        
        queue.async(execute: workItem)
        workItem.cancel()
    }
}

//let propertyDispWorkItem = DispatchWorkItem2()
//propertyDispWorkItem.create()


var view = UIView(frame: CGRect(x: 0, y: 0, width: 400, height: 400))
var photoImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))

photoImage.center = view.center
photoImage.backgroundColor = .yellow
photoImage.contentMode = .scaleAspectFit
view.addSubview(photoImage)

PlaygroundPage.current.liveView = view

let imageURL = URL(string: "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!

//MARK: - classic variant
func fetchImage() {
    let queue = DispatchQueue.global(qos: .utility)
    queue.async {
        if let data = try? Data(contentsOf: imageURL) {
            DispatchQueue.main.async {
                photoImage.image = UIImage(data: data)
            }
        }
    }
}

//fetchImage()

//MARK: - Dispatch work item
func fetchImageWorkItem() {
    var data: Data?
    let queue = DispatchQueue.global(qos: .utility)
    
    let workItem = DispatchWorkItem(qos: .userInteractive) {
        data = try? Data(contentsOf: imageURL)
        print(Thread.current)
        
    }
    
    queue.async(execute: workItem)
    workItem.notify(queue: DispatchQueue.main) {
        if let imageData = data {
            photoImage.image = UIImage(data: imageData)
            print(Thread.current)
        }
    }
}

//fetchImageWorkItem()

//MARK: - The third variant(Async URLSession)
func fetchImageURL() {
    let task = URLSession.shared.dataTask(with: imageURL) { data, response, error in
        print(Thread.current)
        if let imageData = data {
            DispatchQueue.main.async {
                print(Thread.current)
                sleep(5)
                photoImage.image = UIImage(data: imageData)
            }
        }
    }
    task.resume()
}

fetchImageURL()
