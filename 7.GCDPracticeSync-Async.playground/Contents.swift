import UIKit
import PlaygroundSupport

class MyViewController: UIViewController {
    var button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "vc 1"
        
        self.view.backgroundColor = .white
        button.addTarget(self, action: #selector(pressAction), for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        initButton()
    }
    
    func initButton() {
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        button.center = view.center
        button.setTitle("TAP THIS", for: .normal)
        button.backgroundColor = .green
        button.layer.cornerRadius = 10
        button.setTitleColor(.white, for: .normal)
        view.addSubview(button)
    }
    
    @objc func pressAction() {
//        print("Pressed")
        let newVc = MySecondViewController()
        self.navigationController?.pushViewController(newVc, animated: true)
    }
    
}

class MySecondViewController: UIViewController {
    
    var image = UIImageView()
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "vc 2"
        
        self.view.backgroundColor = .white
        
        loadPhoto()
        
// ----------WRONG DOWN CODE, BE CAREFUL-----------
        
//        let imageURL: URL = URL(string: "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")! // one main thread, all will have stoped while image download to project
//        if let data = try? Data(contentsOf: imageURL) {
//            self.image.image = UIImage(data: data)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        initImage()
        initLabel()
    }
    
    func initImage() {
        image.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        image.center = view.center
        view.addSubview(image)
    }
    
    func initLabel() {
        label.frame = CGRect(x: 100, y: 100, width: 150, height: 150)
        label.text = "SOMETHING"
        view.addSubview(label)
    }
    
    func loadPhoto() {
        let imageURL: URL = URL(string: "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!
        let queue = DispatchQueue.global(qos: .utility)
        queue.async { // if will queue.sync and .main.sync = Dead Lock
            if let data = try? Data(contentsOf: imageURL) {
                sleep(10)  // Test my async queue
                DispatchQueue.main.async {
                    self.image.image = UIImage(data: data)
                }
            }
        }
    }
}

let vc = MyViewController()
let navbar = UINavigationController(rootViewController: vc)
navbar.view.frame = CGRect(x: 0, y: 0, width: 320, height: 560)

PlaygroundPage.current.liveView = navbar
