import UIKit
import Darwin

var thread = pthread_t(bitPattern: 0) // create a thread
var attribute = pthread_attr_t()

pthread_attr_init(&attribute)
pthread_create(&thread, &attribute, { (pointer) in
    print("test1")
    return nil
}, nil)

// For normal Users from obj-c Thread

var nsThread = Thread {
    print("test2")
}

nsThread.start()
nsThread.cancel()
print(nsThread.isMainThread)
