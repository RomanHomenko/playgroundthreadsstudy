import UIKit

var availble = false
var condition = pthread_cond_t()
var mutex = pthread_mutex_t()

class ConditionMutexPrinter: Thread {
    
    override init() {
        pthread_cond_init(&condition, nil)
        pthread_mutex_init(&mutex, nil)
    }
    
    override func main() {
        printerMethod()
    }
    
    private func printerMethod() {
        pthread_mutex_lock(&mutex)
        print("printer enter")
        while (!availble) {
            pthread_cond_wait(&condition, &mutex)
        }
        availble = false
        defer {
            pthread_mutex_unlock(&mutex)
        }
        
        print("printer exit")
    }
    
}

class ConditionMutexWriter: Thread {
    
    override init() {
        pthread_cond_init(&condition, nil)
        pthread_mutex_init(&mutex, nil)
    }
    
    override func main() {
        writerMethod()
    }
    
    private func writerMethod() {
        pthread_mutex_lock(&mutex)
        print("writer enter")
        availble = true
        pthread_cond_signal(&condition)
        defer {
            pthread_mutex_unlock(&mutex)
        }
        
        print("writer exit")
    }
    
}

//let conditionMutexWriter = ConditionMutexWriter()
//let conditionMutexPrinter = ConditionMutexPrinter()
//
//conditionMutexWriter.start()
//conditionMutexPrinter.start()

let conditionN = NSCondition()
var availableE = false

class WriterThreadL: Thread {
    
    static var arr = [2, 3, 4, 5]
    
    override func main() {
        conditionN.lock()
        print("write enter")
        WriterThreadL.arr.append(6)
        availableE = true
        conditionN.signal()
        defer {
            conditionN.unlock()
        }
        print("write exit")
    }
}

class PrinterThreadL: Thread {
    
    override func main() {
        conditionN.lock()
        print("printer enter")
        print(WriterThreadL.arr)
        while (!availableE) {
            conditionN.wait()
        }
//        while true {
//            print("HEY")
//            PrinterThreadL.sleep(forTimeInterval: 4)
//        }
        availableE = false
        defer {
            conditionN.unlock()
        }
        print(WriterThreadL.arr)
        print("printer exit")
    }
}

let writer = WriterThreadL()
let printer = PrinterThreadL()

printer.start()
writer.start()
//printer.start()


