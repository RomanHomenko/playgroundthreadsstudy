//
//  ViewController.swift
//  8.GCDAsyncAfterConcurrentPerformInitiallyInactive
//
//  Created by Роман Хоменко on 07.10.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        afterBlock(seconds: 4, queue: DispatchQueue.main) { // UI Logic only in main queue
            print("HEY!")
            self.showAlert()
            print(Thread.current)
        }
        
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "My test", message: "This is ok?", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
    }

    func afterBlock(seconds: Int, queue: DispatchQueue = DispatchQueue.global(), completion: @escaping() -> ()) {
        queue.asyncAfter(deadline: .now() + .seconds(seconds)) {
            completion()
        }
    }

}

