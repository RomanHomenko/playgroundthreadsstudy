//
//  SecondViewController.swift
//  8.GCDAsyncAfterConcurrentPerformInitiallyInactive
//
//  Created by Роман Хоменко on 07.10.2021.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let arr = [1, 2, 3, 4, 5]
        myInactiveQueue(arr: arr)
    }
    
    func myInactiveQueue(arr: [Int]) {
        let inactiveQueue = DispatchQueue(label: "Inactive", attributes: [.concurrent, .initiallyInactive])
        var ARR = arr
        
        // Should always be Active() or Resume() after pause, otherwise it won't work
        inactiveQueue.async {
            print(ARR)
            ARR.append(6)
            print("THREAD LOGIC")
            print(ARR)
        }
        print("Has not yet start...")
        inactiveQueue.activate() // Or that in the condition
        print("Active!")
        inactiveQueue.suspend()
        print("Pause!!!")
        inactiveQueue.resume() // Or that the last condition
        print("Resume queue")
    }

}
