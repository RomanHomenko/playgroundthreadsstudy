import UIKit

// Serial queues
// - -> - -> - -> - -> - -> - -> -

// Concurrent queues
// - - - - - - -
// - - - - - - -
// - - - - - - -


// Sync        Async
// 1 --  -- -  1 - --- - --
// 2   --  -   2 -- - -- -

class QueueTest1 {
    private let serialQueue = DispatchQueue(label: "serialTest1")
    private var cuncurrnetQueue = DispatchQueue(label: "cuncurrentQueueTest1", attributes: .concurrent)
}

class QueueTest2 {
    private let globalQueue = DispatchQueue.global()
    private let mainQueue = DispatchQueue.main
}

