import UIKit
import PlaygroundSupport
//
PlaygroundPage.current.needsIndefiniteExecution = true
//
//let queue = DispatchQueue(label: "SemaphoreQueue", attributes: .concurrent)
//let semaphore = DispatchSemaphore(value: 0) // (value: 5) = number of queues
//
//queue.async {
//    semaphore.wait() // -1(decriment)
//    sleep(3)
//    print("method 1")
//    semaphore.signal() // +1 increment semaphore after all logic
//}
//
//queue.async {
//    semaphore.wait() // -1(decriment)
//    sleep(3)
//    print("method 2")
//    semaphore.signal() // +1 increment semaphore after all logic
//}
//
//queue.async {
//    semaphore.wait() // -1(decriment)
//    sleep(3)
//    print("method 3")
//    semaphore.signal() // +1 increment semaphore after all logic
//}
//
//let sem = DispatchSemaphore(value: 0)
//
//DispatchQueue.concurrentPerform(iterations: 10) { (id: Int) in
//    sem.wait(timeout: DispatchTime.distantFuture)
//    sleep(1)
//    print("Block", String(id))
//
//    sem.signal()
//}

class SemaphoreTest {
    private let semaphore = DispatchSemaphore(value: 1)
    private var array = [Int]()
    
    private func methodWork(_ id: Int) {
        semaphore.wait() // -1
        array.append(id)
        print(array)
        sleep(1)
        semaphore.signal() // +1
    }
    
    public func startAllThreads() {
        DispatchQueue.global().async {
            self.methodWork(123)
        }
        DispatchQueue.global().async {
            self.methodWork(321)
        }
        DispatchQueue.global().async {
            self.methodWork(231)
        }
        DispatchQueue.global().async {
            self.methodWork(132)
        }
        DispatchQueue.global().async {
            self.methodWork(333)
        }
    }
}

let semaphoreTest = SemaphoreTest()
semaphoreTest.startAllThreads()
